package ioutil

import (
	"io"
	"time"
)

// Deadliner allows a deadline to be set for reading and writing
type Deadliner interface {
	SetDeadline(time.Time) error
}

// ReadDeadliner allows a deadline to be set for reading
type ReadDeadliner interface {
	SetReadDeadline(time.Time) error
}

// WriteDeadliner allows a deadline to be set for writing
type WriteDeadliner interface {
	SetWriteDeadline(time.Time) error
}

// DeadlineReadWriter implements io.ReadWriter, Deadliner, ReadDeadliner, and WriteDeadliner
type DeadlineReadWriter interface {
	io.ReadWriter
	Deadliner
	ReadDeadliner
	WriteDeadliner
}

// DeadlineReader implements io.Reader and ReadDeadline
type DeadlineReader interface {
	io.Reader
	ReadDeadliner
}

// DeadlineWriter implements io.Writer and WriteDeadline
type DeadlineWriter interface {
	io.Writer
	WriteDeadliner
}
