package iochan

import (
	"io"
	"time"

	ioutil "gitlab.com/go-utils/io"
)

// New returns a new ReadWriter for the given read and write channels
func New(r <-chan []byte, w chan<- []byte) *ReadWriter {
	return &ReadWriter{
		Reader{Chan: r},
		Writer{Chan: w},
	}
}

// NewReader returns a new Reader for the given channel
func NewReader(ch <-chan []byte) *Reader {
	return &Reader{Chan: ch}
}

// NewWriter returns a new Writer for the given channel
func NewWriter(ch chan<- []byte) *Writer {
	return &Writer{Chan: ch}
}

// MakeReader creates a WriteChan and returns a Reader and the channel
func MakeReader(n int) (*Reader, chan<- []byte) {
	ch := make(chan []byte, n)
	return NewReader(ch), ch
}

// MakeWriter creates a ReadChan and returns a Writer and the channel
func MakeWriter(n int) (*Writer, <-chan []byte) {
	ch := make(chan []byte, n)
	return NewWriter(ch), ch
}

// Pipe creates a channel and returns a Reader and a Writer for the channel
func Pipe(n int) (*Reader, *Writer) {
	ch := make(chan []byte, n)
	return NewReader(ch), NewWriter(ch)
}

// Reader is a channel-based io.Reader
type Reader struct {
	Chan     ReadChan
	Deadline Deadline
	Rest     []byte
}

var _ ioutil.DeadlineReader = new(Reader)

// SetReadDeadline implements ReadDeadliner
func (r *Reader) SetReadDeadline(t time.Time) error { return r.Deadline.Set(t) }

// SetDeadline implements Deadliner
func (r *Reader) SetDeadline(t time.Time) error { return r.Deadline.Set(t) }

// Read implements io.Read
func (r *Reader) Read(b []byte) (n int, err error) {
	n, r.Rest, err = r.Chan.Read(r.Deadline, r.Rest, b)
	return
}

// Writer is a channel-based io.Writer
type Writer struct {
	Chan     WriteChan
	Deadline Deadline
}

var _ ioutil.DeadlineWriter = new(Writer)
var _ io.Closer = new(Writer)

// SetWriteDeadline implements WriteDeadliner
func (w *Writer) SetWriteDeadline(t time.Time) error { return w.Deadline.Set(t) }

// SetDeadline implements Deadliner
func (w *Writer) SetDeadline(t time.Time) error { return w.Deadline.Set(t) }

// Write implements io.Writer
func (w *Writer) Write(b []byte) (n int, err error) { return w.Chan.Write(w.Deadline, b) }

// Close implements io.Closer
func (w *Writer) Close() error { return w.Chan.Close() }

// ReadWriter is a channel-based io.ReadWriter
type ReadWriter struct {
	Reader
	Writer
}

var _ ioutil.DeadlineReadWriter = new(ReadWriter)
var _ io.Closer = new(ReadWriter)

// SetDeadline implements Deadliner
func (rw *ReadWriter) SetDeadline(t time.Time) error {
	dl := make(Deadline)
	err := dl.Set(t)

	if err == nil {
		rw.Reader.Deadline = dl
		rw.Writer.Deadline = dl
	}

	return err
}
