package iochan

import (
	"errors"
	"fmt"
	"io"
	"time"
)

// TimeoutError is an error caused by a timeout
type TimeoutError struct{ action string }

// Error implements error.Error()
func (e *TimeoutError) Error() string { return fmt.Sprintf("%s timed out", e.action) }

// Timeout implements net.Error.Timeout()
func (e *TimeoutError) Timeout() bool { return true }

// Temporary implements net.Error.Temporary()
func (e *TimeoutError) Temporary() bool { return true }

// Deadline is a channel that can be read after a specified time
type Deadline <-chan time.Time

// Set sets the deadline to the given time
func (l *Deadline) Set(t time.Time) error {
	d := t.Sub(time.Now())
	if d < 0 {
		return errors.New("time is in the past")
	}

	*l = time.After(d)
	return nil
}

// ReadChan is a channel that bytes can be read from
type ReadChan <-chan []byte

// Read reads bytes from the channel
//
// If len(unread) is non-zero, bytes will be copied from unread and the channel
// will not be read.
//
// If len(unread) or len(bytes from channel) is greater than len(b), remainder
// will be the remaining bytes.
func (ch ReadChan) Read(dl Deadline, unread []byte, b []byte) (n int, remainder []byte, err error) {
	for len(unread) == 0 {
		select {
		case <-dl:
			return 0, unread, &TimeoutError{"reading"}
		case d, ok := <-ch:
			if !ok {
				return 0, unread, io.EOF
			}
			unread = d
		}
	}

	n = copy(b, unread)
	if n == len(unread) {
		unread = nil
	} else {
		unread = unread[n:]
	}
	return n, unread, nil
}

// WriteChan is a channel that bytes can be written to
type WriteChan chan<- []byte

// Write writes bytes to the channel
//
// The given slice is copied to a new slice to avoid ownership issues.
func (ch WriteChan) Write(dl Deadline, b []byte) (n int, err error) {
	d := make([]byte, len(b))
	n = copy(d, b)
	if n != len(b) {
		panic("copying failed")
	}

	select {
	case <-dl:
		return 0, &TimeoutError{"writing"}
	case ch <- d:
		return n, nil
	}
}

// WriteUnsafe writes bytes to the channel in an unsafe manner
//
// WriteUnsafe is unsafe because the slice is written directly to the channel.
// This may cause a race between modifications to the slice and reading from the
// slice on the other end of the channel.
func (ch WriteChan) WriteUnsafe(dl Deadline, b []byte) (n int, err error) {
	select {
	case <-dl:
		return 0, &TimeoutError{"writing"}
	case ch <- b:
		return len(b), nil
	}
}

// Close closes the channel
func (ch WriteChan) Close() error {
	close(ch)
	return nil
}
