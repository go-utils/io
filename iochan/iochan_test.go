package iochan_test

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/require"
	iou "gitlab.com/go-utils/io"
	"gitlab.com/go-utils/io/iochan"
)

var deadbeef = []byte{0xDE, 0xAD, 0xBE, 0xEF}

func async(fn func()) chan struct{} {
	done := make(chan struct{})
	go func() {
		fn()
		close(done)
	}()
	return done
}

func TestPipe(t *testing.T) {
	r, w := iochan.Pipe(0)

	done := async(func() {
		n, err := iou.WriteAll(w, deadbeef)
		require.NoError(t, err)
		require.Equal(t, len(deadbeef), n)

		w.Close()
	})

	b, err := ioutil.ReadAll(r)
	require.NoError(t, err)
	require.Equal(t, deadbeef, b)

	<-done
}

func TestRead(t *testing.T) {
	r, w := iochan.MakeReader(0)

	done := async(func() {
		w <- deadbeef
		close(w)
	})

	b, err := ioutil.ReadAll(r)
	require.NoError(t, err)
	require.Equal(t, deadbeef, b)

	<-done
}

func TestWrite(t *testing.T) {
	w, r := iochan.MakeWriter(0)

	done := async(func() {
		n, err := iou.WriteAll(w, deadbeef)
		require.NoError(t, err)
		require.Equal(t, len(deadbeef), n)

		w.Close()
	})

	require.Equal(t, deadbeef, <-r)
	<-done

	_, ok := <-r
	require.False(t, ok)
}

func TestReadRemaining(t *testing.T) {
	r, w := iochan.MakeReader(1)

	w <- deadbeef

	b := []byte{0}
	for i := range deadbeef {
		n, err := r.Read(b)
		require.NoError(t, err)
		require.Equal(t, 1, n)
		require.Equal(t, deadbeef[i], b[0])
	}
}
