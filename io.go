package ioutil

import (
	"io"
)

func WriteAll(w io.Writer, data []byte) (int, error) {
	var N int
	for {
		n, err := w.Write(data)
		N += n
		data = data[n:]

		if len(data) == 0 || err != nil {
			return N, err
		}
	}
}
